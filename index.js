const fs = require('fs');
const path = require('path');
const express = require('express');
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

const port = 8080;

app.use(((req, res, next) => {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Accept', 'application/json');
    console.log(`method: ${req.method}, params: ${req.params}`);
    next();
}))

app.post('/api/files', (req, res) => {
    const extensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];
    if (req.body.filename){
        const splitted = req.body.filename.split('.');
        const extension = splitted[splitted.length-1];

        if (!extensions.includes(extension)){
            return res.status(400).json({
                "message": `Application supports only ${extensions.join(', ')} extensions`
            });
        }
    }

    if (!req.body.filename || !req.body.content) {
        return res.status(400).json({
            "message": "Please specify 'content' parameter"
        });
    }

    try {
        fs.writeFile('files/' + req.body.filename, req.body.content, err => {
            if (err) {
                throw err;
            }
            console.log('Saved');

            res.status(200).json({
                "message": "File created successfully"
            });
        });
    } catch (e) {
        res.status(500).json({
            "message": "Server error"
        })
    }
})

app.get('/api/files', ((req, res) => {

    const filesPath = path.resolve('./files');

    try {
        fs.readdir(filesPath, ((err, files) => {

            console.log(files);

            if (err) {
                console.log(err);
                res.status(400).json({
                    "message": "Client error"
                });
            }
            res.status(200).json({
                "message": "Success",
                files
            });
        }))
    } catch (e) {
        console.log(e);
        res.status(500).json({
            "message": "Server error"
        })
    }

}))

app.get('/api/files/:filename', ((req, res) => {

    const {filename} = req.params;
    const extension = filename.split('.')[filename.split('.').length-1];

    const filesPath = path.resolve('./files');
    const message = 'Success';
    try {
        const uploadedDate = fs.statSync(filesPath + `/${filename}`).ctime;
        const content = fs.readFileSync(filesPath + `/${filename}`, 'utf8');

        try {
            fs.readdir(filesPath, ((err, files) => {

                if (err) {
                    console.log(err);
                }

                res.status(200).json({
                    message,
                    filename,
                    content,
                    extension,
                    uploadedDate
                });
            }))
        }catch (e){
            res.status(500).json({
                "message": "Server error"
            })
        }
    }catch (e){
        res.status(400).json({
            "message": "Client error"
        });
    }
}))

app.listen(port, () => {
    console.log(`Server has been started on port ${port}`);
});
